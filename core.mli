type value = Int of int | Str of string | Chr of char;; (* | ... *)
type name = string;;
type arity = int;;
type variable = int;;
type term = Fun of name * arity * args | Var of variable | Val of value
and args = term list;;
type clause = args * (pred * args) list (* | Fact of term list *)
and pred = { name : name; arity : arity; mutable clauses :  clause list };;
type program = (string * int, pred) Hashtbl.t;;


