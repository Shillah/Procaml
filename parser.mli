open Core.Std;;
val parse_string : string -> Procaml.program;;
val parse_file : string -> Procaml.program;
