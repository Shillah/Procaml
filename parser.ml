open Procaml

let string_to_term str = str
  
     
let partition_into_preds str =
  str
     
let parse_string str =
  partition_into_preds str;;

let load_file (file_name : string) =
  let file = open_in file_name in
  let n = in_channel_length file in
  let s = Bytes.create n in
  really_input file s 0 n;
  close_in file;
  s
                     
let parse_file (file_name : string) =
  load_file file_name
  |>  parse_string;;
         
